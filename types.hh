//
// Created by mikusjar on 3.10.17.
//

#ifndef BACKPACK01_TYPES_H
#define BACKPACK01_TYPES_H

#include <cstdint>

#define PRINT_DEBUG 0

typedef std::uint8_t t_uint8;
typedef std::uint16_t t_uint16;
typedef std::uint32_t t_uint32;
typedef std::uint64_t t_uint64;

typedef std::int8_t t_int8;
typedef std::int16_t t_int16;
typedef std::int32_t t_int32;
typedef std::int64_t t_int64;


#endif //BACKPACK01_TYPES_H
