//
// Created by mikusjar on 7.10.17.
//

#ifndef BACKPACK01_COMMON_DOUBLE_HPP
#define BACKPACK01_COMMON_DOUBLE_HPP

#include <limits>
#include <cmath>
#include <iostream>
#include "types.hh"

#define OP_DECL(retType,op) inline retType operator op ( const Double& x )

#define SIMPLE_CMP_OP(op) 	OP_DECL( bool, op ){ return val op x.val; }
#define NEW_INST_OP(op) 	OP_DECL( Double, op ){ return Double(val op x.val); }
#define MODIFY_OP(op) 		OP_DECL( Double&, op ){ val op x.val; return *this; }

#define CONV_CONS(type)		Double( type val ) : val( (double)val ) {}

/**
 * Java-like Double class to ease up comparing
 */
struct Double {
	constexpr static double EPS = std::numeric_limits< double >::epsilon();
	double val;

	CONV_CONS( const double )
	CONV_CONS( t_uint64 )

	Double operator=( const double x ){
		return Double( x );
	}

	inline bool operator==( const Double& x ) {
		return std::fabs( val - x.val ) < EPS;
	}

	SIMPLE_CMP_OP(<=)
	SIMPLE_CMP_OP(<)
	SIMPLE_CMP_OP(>=)
	SIMPLE_CMP_OP(>)

	NEW_INST_OP(-)
	NEW_INST_OP(+)
	NEW_INST_OP(/)
	NEW_INST_OP(*)

	MODIFY_OP(-=)
	MODIFY_OP(+=)
	MODIFY_OP(/=)
	MODIFY_OP(*=)

	inline Double& operator+=( const double x ){
		val += x;
		return *this;
	}

	operator t_uint64()const{ return (t_uint64)val; }
	operator const double()const{ return val; }

	friend std::ostream& operator << ( std::ostream& os, const Double& x ){
		return os << x.val;
	}

};

#endif //BACKPACK01_COMMON_DOUBLE_HPP
